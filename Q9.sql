﻿--問９. 以下のようなデータを取得するSQLを作成してください。

SELECT 
    t1.category_name,
    SUM(t2.item_price) AS total_price
FROM
    item_category t1
INNER JOIN 
    item t2
ON 
t1.category_id = t2.category_id

GROUP BY
t1.category_id

ORDER BY
total_price DESC;


